package com.example.myplacesapp.viewModels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.data.repositories.MyPlacesRepository
import com.example.myplacesapp.presentation.main.MainViewModel
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MainViewModelTests {

    private val myPlacesRepository: MyPlacesRepository = mock()

    private lateinit var mainViewModel: MainViewModel
    private var data = MutableLiveData<List<Place>>()

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        mainViewModel = MainViewModel(myPlacesRepository)
        whenever(myPlacesRepository.getAllPlaces()).doReturn(data)
    }

    @Test
    fun addPlaceTest() {
        whenever(myPlacesRepository.addPlace(place1)).doAnswer { Completable.complete() }

        mainViewModel.addPlace(place1).test()
            .assertNoErrors()
            .assertNoValues()
    }

    @Test
    fun addPlaceTestWithMissingModel() {
        mainViewModel.addPlace(null).test()
            .assertError(Throwable::class.java)
    }

    @Test
    fun deletePlaceTest() {
        whenever(myPlacesRepository.removePlace(place1)).doAnswer { Completable.complete() }

        mainViewModel.deletePlace(place1).test()
            .assertNoErrors()
            .assertNoValues()
    }

    @Test
    fun deletePlaceTestNoExistPlace() {
        whenever(myPlacesRepository.removePlace(place1)).doAnswer { Completable.error(Throwable("no place")) }

        mainViewModel.deletePlace(place1).test()
            .assertError(Throwable::class.java)
    }

    companion object {
        private var place1 = Place(
            id = "id-1",
            description = "Wroclaw",
            latitude = 21.4,
            longitude = 22.4,
            address = "54-253 Wroclaw, Poland"
        )
    }
}
