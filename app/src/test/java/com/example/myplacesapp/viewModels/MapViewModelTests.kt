package com.example.myplacesapp.viewModels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.data.repositories.MyPlacesRepository
import com.example.myplacesapp.presentation.main.MainViewModel
import com.example.myplacesapp.presentation.map.MapViewModel
import com.example.myplacesapp.repositories.MyPlacesRepositoryTests
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MapViewModelTests{

    private val myPlacesRepository : MyPlacesRepository = mock()

    private lateinit var mapViewModel: MapViewModel
    private var data = MutableLiveData<List<Place>>()

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        mapViewModel = MapViewModel(myPlacesRepository)
    }

    @Test
    fun initializePlaceTest() {
        whenever(myPlacesRepository.getPlace(place1.id)).doAnswer { MutableLiveData(place1) }

        mapViewModel.initializePlace(place1.id).observeForever {
            Assert.assertEquals(it, place1)
        }
    }

    companion object {
        private var place1 = Place(
            id = "id-1",
            description = "Wroclaw",
            latitude = 21.4,
            longitude = 22.4,
            address = "54-253 Wroclaw, Poland"
        )
    }
}
