package com.example.myplacesapp.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.myplacesapp.data.MyPlacesDatabase
import com.example.myplacesapp.data.dao.PlaceDao
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.data.repositories.MyPlacesRepository
import com.example.myplacesapp.data.repositories.MyPlacesRepositoryImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MyPlacesRepositoryTests {

    private val mockPlaceDao: PlaceDao = mock()

    private val myPlacesDatabase: MyPlacesDatabase = mock {
        on { it.placeDao } doReturn mockPlaceDao
    }

    private lateinit var myPlacesRepository: MyPlacesRepository

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        myPlacesRepository = MyPlacesRepositoryImpl(myPlacesDatabase)
    }

    @Test
    fun getPlacesTest() {
        whenever(myPlacesDatabase.placeDao.getPlaces()).doAnswer {
            MutableLiveData(listOf(place1, place2, place3))
        }

        myPlacesRepository.getAllPlaces().observeForever {
            assertEquals(listOf(place1, place2, place3), it)
        }
    }

    @Test
    fun getPlaceTest() {
        whenever(myPlacesDatabase.placeDao.getPlace(place1.id)).doAnswer {
            MutableLiveData(place1)
        }

        myPlacesRepository.getPlace(place1.id).observeForever {
            assertEquals(place1, it)
        }
    }

    @Test
    fun insertPlaceTest() {
        whenever(myPlacesDatabase.placeDao.insertPlace(place1)).doAnswer { Completable.complete() }

        myPlacesRepository.addPlace(place1)
            .test()
            .assertNoErrors()
            .assertNoValues()
    }

    @Test
    fun deletePlaceTest() {
        whenever(myPlacesDatabase.placeDao.deletePlace(place1)).doAnswer { Completable.complete() }

        myPlacesRepository.removePlace(place1)
            .test()
            .assertNoErrors()
            .assertNoValues()
    }

    companion object {
        private var place1 = Place(
            id = "id-1",
            description = "Wroclaw",
            latitude = 21.4,
            longitude = 22.4,
            address = "54-253 Wroclaw, Poland"
        )
        var place2 = Place(
            id = "id-2",
            description = "Warsaw",
            latitude = 21.4,
            longitude = 22.4,
            address = "1-100 Warsaw, Poland"
        )
        private var place3 = Place(
            id = "id-3",
            description = "Krakow",
            latitude = 21.4,
            longitude = 22.4,
            address = "20-211 Krakow, Poland"
        )
    }
}
