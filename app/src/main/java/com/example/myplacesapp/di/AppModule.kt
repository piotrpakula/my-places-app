package com.example.myplacesapp.di

import com.example.myplacesapp.data.MyPlacesDatabase
import com.example.myplacesapp.data.repositories.MyPlacesRepository
import com.example.myplacesapp.data.repositories.MyPlacesRepositoryImpl
import com.example.myplacesapp.presentation.main.MainViewModel
import com.example.myplacesapp.presentation.map.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module(override = true) {

    viewModel { MainViewModel(get()) }

    viewModel { MapViewModel(get()) }

    single { MyPlacesRepositoryImpl(get()) as MyPlacesRepository }

    single { MyPlacesDatabase.getInstance(get()) }
}