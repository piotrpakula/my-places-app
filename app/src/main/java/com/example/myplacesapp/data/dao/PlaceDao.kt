package com.example.myplacesapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.myplacesapp.data.entitles.Place
import io.reactivex.Completable

@Dao
interface PlaceDao {

    @Query("SELECT * FROM places WHERE id == :id")
    fun getPlace(id: String): LiveData<Place>

    @Query("SELECT * FROM places")
    fun getPlaces(): LiveData<List<Place>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertPlace(vararg place: Place): Completable

    @Delete
    fun deletePlace(vararg place: Place) : Completable
}