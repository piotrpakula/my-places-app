package com.example.myplacesapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myplacesapp.data.MyPlacesDatabase.Companion.DatabaseVersion
import com.example.myplacesapp.data.dao.PlaceDao
import com.example.myplacesapp.data.entitles.Place

@Database(
    entities = [Place::class],
    version = DatabaseVersion,
    exportSchema = false
)
abstract class MyPlacesDatabase : RoomDatabase() {

    abstract val placeDao: PlaceDao

    companion object {

        const val DatabaseName = "my_places_database.db"
        const val DatabaseVersion = 1

        private var instance: MyPlacesDatabase? = null

        fun getInstance(context: Context): MyPlacesDatabase = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(
                context,
                MyPlacesDatabase::class.java,
                DatabaseName
            ).build().also { instance = it }
        }
    }
}