package com.example.myplacesapp.data.entitles

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "places")
data class Place(
    @ColumnInfo(name = "id") @PrimaryKey var id: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "latitude") var latitude: Double,
    @ColumnInfo(name = "longitude") var longitude: Double,
    @ColumnInfo(name = "address ") var address: String
)