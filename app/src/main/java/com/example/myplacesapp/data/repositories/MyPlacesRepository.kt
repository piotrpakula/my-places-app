package com.example.myplacesapp.data.repositories

import androidx.lifecycle.LiveData
import com.example.myplacesapp.data.entitles.Place
import io.reactivex.Completable

interface MyPlacesRepository {

    fun getAllPlaces(): LiveData<List<Place>>

    fun getPlace(id: String): LiveData<Place>

    fun addPlace(place: Place): Completable

    fun removePlace(place: Place): Completable
}