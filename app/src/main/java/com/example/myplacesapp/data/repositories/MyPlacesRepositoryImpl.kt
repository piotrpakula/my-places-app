package com.example.myplacesapp.data.repositories

import androidx.lifecycle.LiveData
import com.example.myplacesapp.data.MyPlacesDatabase
import com.example.myplacesapp.data.entitles.Place
import io.reactivex.Completable

class MyPlacesRepositoryImpl(private val myPlacesDatabase: MyPlacesDatabase) : MyPlacesRepository {

    override fun getAllPlaces(): LiveData<List<Place>> {
        return myPlacesDatabase.placeDao.getPlaces()
    }

    override fun getPlace(id: String): LiveData<Place> {
        return myPlacesDatabase.placeDao.getPlace(id)
    }

    override fun addPlace(place: Place): Completable {
        return myPlacesDatabase.placeDao.insertPlace(place)
    }

    override fun removePlace(place: Place): Completable {
        return myPlacesDatabase.placeDao.deletePlace(place)
    }
}