package com.example.myplacesapp

import android.app.Application
import com.example.myplacesapp.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyPlacesApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyPlacesApp)
            modules(applicationModule)
        }
    }
}