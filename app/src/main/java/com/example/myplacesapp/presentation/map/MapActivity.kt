package com.example.myplacesapp.presentation.map

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.Observer
import com.example.myplacesapp.R
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.databinding.ActivityMapBinding
import com.example.myplacesapp.presentation.main.MainActivity.Companion.INTENT_ID_KEY
import com.example.myplacesapp.shared.toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapActivity : AppCompatActivity() {

    private val viewModel: MapViewModel by viewModel()
    private val map: SupportMapFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityMapBinding>(this, R.layout.activity_map)

        val placeId = intent.extras?.getString(INTENT_ID_KEY) ?: ""

        viewModel.initializePlace(placeId).observe(this, Observer<Place> { place ->
            if (place != null) {
                map.getMapAsync { map ->
                    val latLng = LatLng(place.latitude, place.longitude)
                    map.addMarker(MarkerOptions().position(latLng).title(place.description))
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    map.animateCamera( CameraUpdateFactory.zoomTo( 12.0f ) )
                }
            } else {
                toast(getString(R.string.error_invalid_id)).show()
            }
        })
    }
}