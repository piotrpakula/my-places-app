package com.example.myplacesapp.presentation.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.Observer
import com.example.myplacesapp.BuildConfig
import com.example.myplacesapp.R
import com.example.myplacesapp.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.shared.Mappers.map
import com.example.myplacesapp.presentation.map.MapActivity
import com.example.myplacesapp.shared.toast
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.api.model.Place as GooglePlace
import com.google.android.libraries.places.api.model.Place.Field.NAME
import com.google.android.libraries.places.api.model.Place.Field.ADDRESS
import com.google.android.libraries.places.api.model.Place.Field.ID
import com.google.android.libraries.places.api.model.Place.Field.LAT_LNG
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val disposeBag = CompositeDisposable()
    private val autocomplete: AutocompleteSupportFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.autocompleate_fragment) as AutocompleteSupportFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        val adapter = PlaceAdapter(
            onClickItem = navigateToMap(),
            onClickDelete = deletePlace()
        )

        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.channels.adapter = adapter

        subscribeUi(adapter)
        initializeGooglePlacesApi()

        autocomplete.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(googlePlace: GooglePlace) {
                disposeBag.add(viewModel.addPlace(map(googlePlace))
                    .subscribeOn(Schedulers.io())
                    .observeOn(mainThread())
                    .subscribe({
                        toast(getString(R.string.place_added)).show()
                    }, {
                        toast(it.message.toString()).show()
                    })
                )
            }

            override fun onError(status: Status) {
                toast(status.statusMessage.toString()).show()
            }
        })
    }

    override fun onDestroy() {
        disposeBag.clear()
        super.onDestroy()
    }

    private fun subscribeUi(adapter: PlaceAdapter) {
        viewModel.myPlaces.observe(this, Observer<List<Place>> { value ->
            adapter.submitList(value)
        })
    }

    private fun navigateToMap(): (Place) -> Unit = { place ->
        val intent = Intent(this, MapActivity::class.java)
            .putExtra(INTENT_ID_KEY, place.id)
        startActivity(intent)
    }

    private fun deletePlace(): (Place) -> Unit = { place ->
        disposeBag.add(viewModel.deletePlace(place)
            .subscribeOn(Schedulers.io())
            .observeOn(mainThread())
            .subscribe({
                toast(getString(R.string.place_deleted)).show()
            }, {
                toast(it.message.toString()).show()
            })
        )
    }

    private fun initializeGooglePlacesApi() {
        if (!Places.isInitialized()) Places.initialize(this, BuildConfig.GOOGLE_PLACES_API_KEY)
        Places.createClient(this)

        autocomplete.setPlaceFields(listOf(ADDRESS, ID, LAT_LNG, NAME))
    }

    companion object {
        const val INTENT_ID_KEY = "intent.id.key"
    }
}