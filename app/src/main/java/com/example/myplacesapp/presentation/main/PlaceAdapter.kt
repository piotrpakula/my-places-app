package com.example.myplacesapp.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myplacesapp.R
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.databinding.ItemPlaceBinding


class PlaceAdapter(val onClickItem: (Place) -> Unit, val onClickDelete: (Place) -> Unit) :
    ListAdapter<Place, PlaceAdapter.ViewHolder>(PlaceDiffCallback()) {

    var lastClickTime = 0L

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_place,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            bind(getItem(position), onClickItem, onClickDelete)
            lastClickItemTime = lastClickTime
        }
    }

    class ViewHolder(
        private val binding: ItemPlaceBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        var lastClickItemTime: Long = 0L

        fun bind(place: Place, onClickItem: (Place) -> Unit, onClickDelete: (Place) -> Unit) {
            binding.channelContainer.setOnClickListener { debounce { onClickItem(place) } }
            binding.deleteBtn.setOnClickListener { debounce { onClickDelete(place) } }
            with(binding) {
                vm = place
                executePendingBindings()
            }
        }

        private fun debounce(callback: () -> Unit) {
            val lastClickTime = lastClickItemTime
            val now = System.currentTimeMillis()
            lastClickItemTime = now
            if (now - lastClickTime > 1000L) {
                callback()
            }
        }
    }
}

private class PlaceDiffCallback : DiffUtil.ItemCallback<Place>() {

    override fun areItemsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem == newItem
    }
}