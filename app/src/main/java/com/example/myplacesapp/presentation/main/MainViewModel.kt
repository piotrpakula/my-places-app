package com.example.myplacesapp.presentation.main

import androidx.lifecycle.ViewModel
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.data.repositories.MyPlacesRepository
import io.reactivex.Completable

class MainViewModel(private val myPlacesRepository: MyPlacesRepository) : ViewModel() {

    val myPlaces = myPlacesRepository.getAllPlaces()

    fun addPlace(place: Place?): Completable {
        return place
            ?.let { myPlacesRepository.addPlace(it) }
            ?: Completable.error(Throwable("Missed Id"))
    }

    fun deletePlace(place: Place): Completable {
        return myPlacesRepository.removePlace(place)
    }
}
