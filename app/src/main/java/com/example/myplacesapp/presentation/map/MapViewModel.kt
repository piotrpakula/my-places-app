package com.example.myplacesapp.presentation.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myplacesapp.data.entitles.Place
import com.example.myplacesapp.data.repositories.MyPlacesRepository

class MapViewModel(private val myPlacesRepository: MyPlacesRepository) : ViewModel() {

    fun initializePlace(id: String): LiveData<Place> {
        return myPlacesRepository.getPlace(id)
    }
}