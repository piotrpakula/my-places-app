package com.example.myplacesapp.shared

import com.example.myplacesapp.data.entitles.Place
import com.google.android.libraries.places.api.model.Place as GooglePlace

object Mappers {

    fun map(googlePlace: GooglePlace): Place? {
        googlePlace.id ?: return null

        return Place(
            id = googlePlace.id.toString(),
            description = googlePlace.name.toString(),
            latitude = googlePlace.latLng?.latitude ?: 0.0,
            longitude = googlePlace.latLng?.longitude ?: 0.0,
            address = googlePlace.address.toString()
        )
    }
}
