package com.example.myplacesapp.instrumentation

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.myplacesapp.data.MyPlacesDatabase
import com.example.myplacesapp.data.entitles.Place
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
class PlacesDaoTests {

    private lateinit var db: MyPlacesDatabase
    private var place1 = Place(
        id = "id-1",
        description = "Wroclaw",
        latitude = 21.4,
        longitude = 22.4,
        address = "54-253 Wroclaw, Poland"
    )
    private var place2 = Place(
        id = "id-2",
        description = "Warsaw",
        latitude = 21.4,
        longitude = 22.4,
        address = "1-100 Warsaw, Poland"
    )
    private var place3 = Place(
        id = "id-3",
        description = "Krakow",
        latitude = 21.4,
        longitude = 22.4,
        address = "20-211 Krakow, Poland"
    )

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, MyPlacesDatabase::class.java)
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        db.placeDao.insertPlace(place1, place2, place3).test()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun testGetPlace() {
        val place = getValue(db.placeDao.getPlace(place1.id))
        Assert.assertEquals(place1, place)
    }

    @Test
    fun testGetPlaces() {
        val place = getValue(db.placeDao.getPlaces())
        Assert.assertEquals(3, place.size)
    }

    @Test
    fun testInsertPlace() {
        val newPlace = Place(
            id = "id-4",
            description = "Test Place",
            latitude = 21.4,
            longitude = 22.4,
            address = "Test Address"
        )

        db.placeDao.insertPlace(newPlace).test()
            .assertNoErrors()
            .assertNoValues()

        val savedData = getValue(db.placeDao.getPlace(newPlace.id))
        Assert.assertEquals(newPlace, savedData)
    }

    @Test
    fun testInsertExistingPlace() {
        val newPlace = Place(
            id = "id-4",
            description = "Test Place",
            latitude = 21.4,
            longitude = 22.4,
            address = "Test Address"
        )

        db.placeDao.insertPlace(newPlace).test()
            .assertNoErrors()
            .assertNoValues()

        db.placeDao.insertPlace(newPlace).test()
            .assertError(SQLiteConstraintException::class.java)
    }

    @Test
    fun testDeletePlace() {
        db.placeDao.deletePlace(place1).test()
            .assertNoErrors()
            .assertNoValues()

        val removedPlace = getValue(db.placeDao.getPlace(place1.id))
        Assert.assertNull(removedPlace)
    }
}